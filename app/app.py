from flask import Flask, request, jsonify

app = Flask(__name__)
  
@app.route('/devops', methods=['POST'])
def devops():
    
  json_data = request.json
  to = json_data['to']
  key = request.headers.get('API-Key')
  
  if key == '2f5ae96c-b558-4c7b-a590-a501ae1c3f6c':
        message = "Hello " + to + " your message will be send"
        return jsonify({"message" :  message})
  else:
        return jsonify({"message": "ERROR: Unauthorized"}), 401

app.run(host='0.0.0.0', port=80)